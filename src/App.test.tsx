import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('Renders id', () => {
  render(<App />);
  const linkElement = screen.queryByText(/Сансара/i);
  expect(linkElement).toBeNull();
  const button = screen.getByText(/Турниры/i);
  button.click();
  const trueElement = screen.getByText(/Сансара/i);
  expect(trueElement).toBeInTheDocument();
});
