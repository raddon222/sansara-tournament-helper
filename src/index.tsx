import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
declare var global: {
  STH: {
    index: () => void
  }
}
global.STH = Object.assign(global.STH || {}, {
  init: () => ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
    document.getElementById('sth-root')
  )
});