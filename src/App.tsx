import React, { useState } from 'react';
import './App.css';
import LinkHandler from './component/LinkHandler';
import PopupLayout from './layout/PopupLayout';

const App = () => {
  const [isOpen, setOpen] = useState(false);
  return (
    <div className="STH-holder">
      <button className="STH-main-trigger" onClick={() => setOpen(!isOpen)}>Турниры</button>
      {isOpen && <PopupLayout onClose={() => setOpen(false)}>
        <LinkHandler />
      </PopupLayout>}
    </div>
  );
}

export default App;
