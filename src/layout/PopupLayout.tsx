import React from 'react';
import './PopupLayout.css';

interface PopupLayoutProps extends React.PropsWithChildren<{}> {
  onClose?: () => void
}

const PopupLayout = (props: PopupLayoutProps) => {
  return (
    <div className="STHApp-container">
      <header className="STHApp-close" onClick={props.onClose}>X</header>
      <div className="STHApp">
        <div className="STHApp-content">{props.children}</div>
        <footer className="STHApp-footer">
          <p className="STHApp-copyright">Собственность синдиката "Сансара"®</p>
        </footer>
      </div>
    </div>
  );
}

export default PopupLayout;
