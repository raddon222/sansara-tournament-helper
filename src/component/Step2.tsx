import React from 'react';
import { BattleData } from '../service/tournaments';

interface Step2Props extends React.PropsWithChildren<{}> {
  progress: BattleData[];
  total: string[];
}

const Step2 = (props: Step2Props) => {
  const perc = props.total.length ? (props.progress.length / props.total.length) * 100 : 100;
  return (
    <div className="STHStep2">
      <h1>Шаг 2.</h1>
      <h2>Просто подождите...</h2>
      <div className="STH-text">
        {props.progress.length} / {props.total.length}
      </div>
      <div className="STH-Progressbar" style={{ width: `${perc}%` }} />
    </div>
  );
}

export default Step2;
