import React from 'react';
import { BattleData, invMatch } from '../service/tournaments';

interface Step3Props extends React.PropsWithChildren<{}> {
  results: BattleData[]
}
const copyToClipboard = (str: string) => {
  const clear = str.replaceAll(/(\t)+/g, "\t");
  const el = document.createElement('textarea');
  el.value = clear.replaceAll(/(\t)+/g, "\t").replaceAll(/(\t)+/g, "\t");
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
};

const Step3 = (props: Step3Props) => {
  const doCopy = () => {
    const content = props.results
      .filter(item => item.status === "success").map(item => [
        item.id,
        item.url,
        item.time,
        item.winner,
        item.guild1,
        item.god1,
        item.hero1,
        item.hp1,
        item.hpm1,
        item.inv1,
        item.guild2,
        item.god2,
        item.hero2,
        item.hp2,
        item.hpm2,
        item.inv2,].map(a => (a && a.replaceAll(/[\n\r\t]/g, " ")) || " ").join("\t")
      ).join("\n");
    copyToClipboard(content + "\n");
  }
  return (
    <div className="STHStep3">
      <h1>Шаг 3.</h1>
      <h2>Готово! Забирайте данные!</h2>
      <h2>(Трофеи {Object.keys(invMatch).map(
        (item) => `${item}${invMatch[item] === 1 ? '' : ' x' + invMatch[item].toString()}`
      ).join(", ")})</h2>
      <div className="STHTable-cover">
        <table className="STHTable">
          <thead>
            <tr className="STH-text">
              <th>Статус</th>
              <th>ID лога</th>
              <th>Ссылка</th>
              <th>Время</th>
              <th>Победитель</th>

              <th>Гильд1</th>
              <th>Бог1</th>
              <th>Герой1</th>
              <th>ХП1</th>
              <th>Максхп1</th>
              <th>Инвентарь1</th>

              <th>Гильд2</th>
              <th>Бог2</th>
              <th>Герой2</th>
              <th>ХП2</th>
              <th>Максхп2</th>
              <th>Инвентарь2</th>
            </tr>

          </thead>
          <tbody>
            {props.results.map(item => <tr className="STH-text" key={item.id}>
              <td>{item.status === "success" ? item.status : `${item.status} ${item.reason}`}</td>
              <td>{item.id}</td>
              <td><a target="_blank" rel="noreferrer" href={item.url}>{item.url}</a></td>
              <td>{item.time}</td>
              <td>{item.winner}</td>

              <td>{item.guild1}</td>
              <td>{item.god1}</td>
              <td>{item.hero1}</td>
              <td>{item.hp1}</td>
              <td>{item.hpm1}</td>
              <td>{item.inv1}</td>

              <td>{item.guild2}</td>
              <td>{item.god2}</td>
              <td>{item.hero2}</td>
              <td>{item.hp2}</td>
              <td>{item.hpm2}</td>
              <td>{item.inv2}</td>
            </tr>)}
          </tbody>
        </table>
      </div>
      <button onClick={doCopy} className="STHSubmit" >Скопировать (Успешные)</button>
    </div>
  );
}

export default Step3;
