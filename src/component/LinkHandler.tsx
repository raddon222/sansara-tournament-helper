import React, { useState } from 'react';
import './LinkHandler.css';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import * as Tournament from '../service/tournaments';

interface LinkHandlerProps extends React.PropsWithChildren<{}> {
}

const parse = (current: string[], done: Tournament.BattleData[], update: Function, next: Function, finish: Function) => {
  if (current.length === 0) {
    finish();
    return;
  }
  const item = current.shift() || "";
  if (item.length === 0) {
    finish();
    return;
  }
  Tournament.tournament(item).then((battle) => {
    done.push(battle);
    update(done);
    setTimeout(() => next(current, done, update, next, finish), 2000);
  }).catch((err) => {
    setTimeout(() => next(current, done, update, next, finish), 2000);
  });
};

const LinkHandler = (props: LinkHandlerProps) => {
  const [step, setStep] = useState(1);
  const [linkData, setLinkData] = useState<string[]>([]);
  const [currentBattles, setCurrentBattles] = useState<Tournament.BattleData[]>([]);

  const Step1Submit = (links: string) => {
    const parsed = Tournament.parseLinks(links);
    setLinkData(parsed);
    setCurrentBattles([]);
    setStep(2);
    parse([...parsed], [], (current: Tournament.BattleData[]) => {
      setCurrentBattles([...current]);
    }, parse, () => {
      setStep(3);
    });
  };
  const Step1Update = (links: string) => {
    const parsed = Tournament.parseLinks(links);
    setLinkData(parsed);
  };
  return (
    <div className="STHLink">
      {(step <= 1) && <Step1 parsed={linkData.length} onSubmit={Step1Submit} onUpdate={Step1Update} />}
      {(step === 2) && <Step2 total={linkData} progress={currentBattles} />}
      {(step === 3) && <Step3 results={currentBattles} />}
    </div>
  );
}

export default LinkHandler;
