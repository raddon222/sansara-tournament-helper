import React, { useState } from 'react';

interface Step1Props extends React.PropsWithChildren<{}> {
  onSubmit: (text: string) => void
  onUpdate: (text: string) => void
  parsed: number;
}

const Step1 = (props: Step1Props) => {
  const [currentText, setText] = useState('');
  return (
    <div className="STHStep1">
      <h1>Шаг 1.</h1>
      <h2>Введите ссылки (по одной на строку)</h2>
      <textarea rows={20} value={currentText} onChange={(event) => { setText(event.target.value); props.onUpdate(event.target.value); }} className="STHLink-text" />
      <button disabled={!props.parsed} onClick={() => props.onSubmit(currentText)} className="STHSubmit" >Обработать ссылки! (Распознано {props.parsed})</button>
    </div>
  );
}

export default Step1;
