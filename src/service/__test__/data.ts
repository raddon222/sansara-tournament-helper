export const TEST_REPORT1 = `
<!DOCTYPE html>
<html>
<head>
	  
<title>Хроника Годвилля</title>	

<script type="text/javascript" src="/javascripts/jquery-1.12.4.min.js" ></script>
<script src="/javascripts/awardj.js?1496979418" type="text/javascript"></script>
<script src="/javascripts/params_parse.js?1568892649" type="text/javascript"></script>

<link href="/stylesheets/common.css?1581510012" media="screen" rel="stylesheet" type="text/css" />
<link href="/stylesheets/common-mini.css?1535888562" media="screen" rel="stylesheet" type="text/css" />
<link href="/stylesheets/progress_bar.css?1299941084" media="screen" rel="stylesheet" type="text/css" />
<link href="/stylesheets/hero_log.css?1607387784" media="screen" rel="stylesheet" type="text/css" />
<META NAME="gvdt" CONTENT="hero_log_n"><script src="/javascripts/nt.js?1509365639" type="text/javascript"></script>
<script src="/javascripts/st.js?1459925068" type="text/javascript"></script><script src="/javascripts/stjq.js?1581510012" type="text/javascript"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-1675968-2"></script>
    <script src="/javascripts/garu.js" type="text/javascript"></script>


<meta name="keywords" content="Годвилль, Godville, Zero Player Game, ZPG, ZRPG, самоиграйка, тамагочи, браузерная игра, божественная комедия"><meta name="description" content="Почувствуйте себя богом: персональный герой уже готов игнорировать ваши желания." />
<link rel="shortcut icon" href="/images/favicon.ico"/>
<script nonce='15c00b5250ddedaabc203b67f8b034fd'>
(function () {
    $(document).ready(function() {
        
        var r = parse_query_string(window.location.href);
        if (r && r["u"] == "1"){
            window.location = removeURLParameter("u", window.location.href);
        }
        
    });
}());
</script>
<script nonce='15c00b5250ddedaabc203b67f8b034fd'>
function to_mobile(){
	$.post('/hero/to_mobile', {}, function(data) {
		location.reload();
	});
}
$(document).ready(function() {
    $('.to_mobile').click(function(){
        to_mobile();
        return false;
    });
    $('.to_top').click(function(e){
        $(window).scrollTop(0);
        return false;
    })
    $('#reload').click(function(e){
        location.reload(true)
        return false;
    });
    $('a[data-rv]').click(function(e){
        var v = $(this).data('rv');
        report_godvoice(v[0],v[1],v[2],v[3]);
        return false;
    })
    $('.n_wn').click(function(e){
        window.open(this.href);
        return false;
    })
	if (typeof(jq_mfp_init) != 'undefined'){
		jq_mfp_init();
	}    
});
</script>

</head>
<body>
<div id="wrap">


<div id="page_wrapper">
	<div class="lastduelpl">
	
		<span><a href="/duels/log/lcfz7bchx">Тренировка</a></span>
	
	</div>
    
	
	<div class='ft'>20.04.2021 12:51 </div>
	
    
	
	<div class="lastduelpl">
		
			
		
	</div>
	
	
	<div id="arena_block" style="float: left; width: 100%;">
			<div id="p_status" style="text-align:center;min-height:15px;"></div>
	<div style="text-align:center;display: none;" id="app_bar">
		
			<span id="app_status">Подключаемся к сервису быстрых обновлений...</span>
			<span title="Скрыть"><a href="#" onclick="Effect.toggle(&quot;app_bar&quot;,'blind',{});; return false;">[X]</a></span>		
		
	</div>





<div class='rpl'>
    <div class='rpl_tc' style='display:none;'></div>
<input id='slider' type="range"  min="1" max="25" value="25" data-role="none"/>
<span id='sback' title='Предыдущий ход (стрелка влево)'>⏪&#xFE0E;</span>
<span id='splay' title='Проигрывание/пауза (Enter)'>&#x25b6;&#xFE0E;</span>
<span id='sps' title='Проигрывание/пауза (Enter)' style='display:none;'>⏸&#xFE0E;</span>
<span id='sfwd' title='Следующий ход (стрелка вправо)'>⏩&#xFE0E;</span>
</div>


<script nonce='15c00b5250ddedaabc203b67f8b034fd'>

$(document).ready(function() {
    var hp = {"-1":{"0":288,"1":288},"0":{"0":288,"1":288},"1":{"0":288,"1":288},"2":{"0":288,"1":256},"3":{"0":258,"1":256},"4":{"0":258,"1":231},"5":{"0":238,"1":231},"6":{"0":238,"1":208},"7":{"0":209,"1":208},"8":{"0":209,"1":178},"9":{"0":189,"1":178},"10":{"0":189,"1":162},"11":{"0":159,"1":162},"12":{"0":159,"1":132},"13":{"0":138,"1":132},"14":{"0":138,"1":109},"15":{"0":122,"1":109},"16":{"0":122,"1":88},"17":{"0":122,"1":86},"18":{"0":122,"1":54},"19":{"0":90,"1":54},"20":{"0":90,"1":35},"21":{"0":69,"1":35},"22":{"0":69,"1":3},"23":{"0":44,"1":3},"24":{"0":44,"1":1},"25":{"0":44,"1":1}};    
    var moves = [];
    var d_maps = [];
    
    var last= parseInt($('#turn_num').text());
    
    
    var play_timer = undefined;
    var cur_turn;
            
	var scroll_to = function(element){
        if (element.length == 0){
            return;
        }

		var elHeight = 0;
		element.each(function() {
		  elHeight += $(this).height();
		});        
        
        var offset = element.offset().top + elHeight;
            if(!element.is(":visible")) {
                element.css({"visibility":"hidden"}).show();
                var offset = element.offset().top;
                element.css({"visibility":"", "display":""});
            }

            var visible_area_start = $(window).scrollTop();
            var visible_area_end = visible_area_start + window.innerHeight;

            if(offset < visible_area_start || offset > visible_area_end){
                 // Not in view so scroll to it
                 $('html,body').animate({scrollTop: offset - window.innerHeight/3}, 300);
                 return false;
            }
            return true;
	}        
    
    var map_scroll = function(){
        
        if ($('.dmh').length == 0){
            return;
        }
        
        var m_width = $('.dmh').width();
        var m_left = $('.dmh').position().left;
        
        var map_width = $('#dmap').width();
        
        var s_left = $('#dmap').scrollLeft();
        if ( s_left < m_left || 
            s_left > m_left
        ) {
            $('#dmap').scrollLeft( m_left - map_width / 2);
//            console.log('dungeon map scrolled ' + s_left + ' ' + m_left);
        }
    }

    
    var is_out_of_view = function(element){
        if (element.length == 0){
            return;
        }
        var elementTop = element.offset().top;
        var elementBottom = element.outerHeight();

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        if (viewportTop > elementTop) {
//        if (elementBottom < viewportBottom ){
            return true;
        }
        else {
            return false;
        }
    }
        
    var add_on_scroll = function(el_id){
        if ($(el_id).length > 0){
            ($(el_id).parent().parent()).addClass('bldn');
        }
    }

    var on_scroll = function(el_id){
        var fixed_mode = undefined;
        var dmap = $($('.bldn')[0]);
        var covered = is_out_of_view($(dmap.parent()));
        if (!dmap.hasClass('block_fixed')){
            if (covered && $('.d_content').innerHeight() > window.innerHeight){
                fixed_mode = true;
            }
        }
        else if (!covered) {
            fixed_mode = false
        }
        if (fixed_mode){
            $('.bldn').each(function(index){
                $(this).addClass('block_fixed');
                $(this).offset({'left':$(this).parent().offset().left});
            });
            $('.hide_on_scroll').hide();
        }
        else if (fixed_mode == false) {
            $('.bldn').each(function(index){
                $(this).removeClass('block_fixed');
                $(this).removeAttr('style');
            });
            $('.hide_on_scroll').show();
        }
    }
    
    add_on_scroll('#dmap');
    add_on_scroll('#alls');
    add_on_scroll('#opps');
    add_on_scroll($('#hpp1').parent().parent().parent());
    add_on_scroll($('#hpp0').parent().parent().parent());

    $(window).on('resize scroll', function() {
        on_scroll();
    });        
    
    
    var draw_map = function(turn_num){
      turn_num = turn_num;
      if (d_maps == undefined || d_maps.length == 0){
        return;
      }
      
      var d_map = d_maps[turn_num];
      if (d_map == undefined){
        return;
      }
      var dm = $('#dmap').empty();
      var move = moves[turn_num];
      for (var y=0;y<d_map.length;y++){
        var l = d_map[y];
        var hl = $('<div></div>').addClass('dml').appendTo(dm);

        hl.css('width', l.length*21);
        for (var x=0;x<l.length;x++){
          var v = l[x];
          var cl = $('<div></div>').css({'left': 21*x});
          if (v != 'f'){
            cl.text(v).addClass('dmc');
            
            if (v == '#'){
              cl.addClass('dmw');
            }
            else if (v == '@'){
              cl.addClass('dmh');
            }
            
          }
          cl.appendTo(hl);
        }
      }
    };
    
    var show_turn = function(turn_num, no_scroll){
        
        if (no_scroll == undefined){
            scroll_to($('.t'+turn_num));
        }        
        
        
        $('.ct').removeClass('ct ctbg');
        $('.t'+turn_num).addClass('ct ctbg');
        
        draw_map(turn_num);


        var thp = hp[turn_num];
        var thpp = hp[turn_num-1];
        
        if (thp){
            for (var key in thp) {
                var hpd = undefined;
                var h = $('#hp'+key);
                if (h.length > 0){
                    h.text(thp[key]);
                }
                if (thpp[key] != undefined){
                    hpd = thp[key] - thpp[key];
                }
                
                var h_p = $('#hpd'+key).text('');
                if (hpd){
                    var h_p = $('#hpd'+key);
                    if (h_p.length > 0){
                        h_p.removeClass('hpd_green');
                        h_p.removeClass('hpd_red');
                        if (hpd > 0){
                            h_p.text('+'+(hpd));
                            h_p.addClass('hpd_green');
                        }
                        else {
                            h_p.text((hpd));
                            h_p.addClass('hpd_red');
                        }
                    }
                }
                
                
                if (h_p.length > 0){
                    if (thp[key] == undefined) {
                        h_p.parent().parent().hide();
                    }
                    else {
                        h_p.parent().parent().show();
                    }
                }

                var hpp = $('#hpp'+key);
                if (hpp.length > 0){
                    var hpm = $('#hpm'+key).text();
                    var prct = (thp[key]/hpm*100).toFixed();
                    hpp.css('width', (100 - prct)+'%');
                    
                    var p = hpp.parent();
                    
                    var pc_color = 'green';
                    if (prct < 30 ){
                       pc_color = 'red'
                    }
                    else if (prct < 70){
                      pc_color = 'yellow'
                    }
                    p.css('background-color', pc_color);
                    p.attr('title', prct+'%')
                }
            }
        }
        
        $('#turn_num').text(turn_num);
        $('#slider').val(turn_num);
        if (!no_scroll){
            update_location("s", turn_num);
        }
        
        map_scroll();        
        if (d_maps.length == 0){
          $('.dmh').removeClass('dmh');
          if (moves.length > 0){
              $('#'+moves[turn_num-1]).addClass('dmh');
          }
        }
        if (turn_num == last){
//            $('#sfwd').addClass('rpld');
            $('.dt').show();
        }
        else {
            // $('#sfwd').removeClass('rpld');
            $('.dt').hide();
        }
        if (turn_num == 1){
            // $('#sback').addClass('rpld');
        }
        else {
            // $('#sback').removeClass('rpld');
        }
        return false;
    }
        
	var stop_replay = function(){
		if (play_timer != undefined){
			clearInterval(play_timer);
			play_timer = undefined;
		}
        $('#sps').hide();
        $('#splay').show();        
	}
    
	var play_step_func = function(){
		show_turn(cur_turn);
		
		if (cur_turn == last){
            stop_replay();
            cur_turn = 1;
			return;
		}
        cur_turn++;
	}
    
    var back_f = function(){
        if (cur_turn== 1){
            cur_turn = last+1;
        }
        stop_replay();
        if (cur_turn > 1){
            cur_turn--;
            show_turn(cur_turn);
        }        
    }
    
	var forward_f = function(){
        if (cur_turn==last){
            cur_turn = 0;
        }				        
		stop_replay();
        if (cur_turn < last){
            cur_turn++;
            show_turn(cur_turn);
        }        
    }
    
    var play_f = function(){
        stop_replay();
        if (cur_turn==last){
            cur_turn = 1;
        }
        play_timer = setInterval(play_step_func, 1000);
        play_step_func();
        $('#splay').hide();
        $('#sps').show();        
    }
    
    
    $('#slider').change(function(e) {
        cur_turn = $(this).val();
        e.preventDefault();
        $('.rpl_tc').hide();
        show_turn(cur_turn);
        return false;
    });
    
    $('#slider').on('input', function(event){
        $('.rpl_tc').show();
        $('.rpl_tc').text(event.target.value);
    });    
    
    $('#sback').click(function(e){
        e.preventDefault();
        back_f();
        return false;
    });
    $('#sfwd').click(function(e){
        e.preventDefault();
        forward_f();
        return false;
    });
    
    $('#splay').click(function(e){
        e.preventDefault();
        play_f();
        return false;
    });
    $('#sps').click(function(e){
        e.preventDefault();
        stop_replay()
        return false;;
    });
    
	$("body").keydown(function(e){
        var handled = false;
		if (!e.ctrlKey && !e.altKey && !e.shiftKey && ((e.keyCode || e.which) == 37)) { // left arrow
            handled = true;
			back_f();
		}
		else if (!e.ctrlKey && !e.altKey && !e.shiftKey && ((e.keyCode || e.which) == 39)){ // right arrow
            handled = true;
			forward_f();
		}
		else if ((e.keyCode || e.which) == 13 && !e.ctrlKey && !e.altKey && !e.shiftKey ){ // enter
            handled = true;
			if ($('#splay').css('display') != 'none'){
				play_f();
			}
			else {
				stop_replay();
			}
		}
        if (handled){
            e.preventDefault();
            return false;
        }
        return true;
	});
    
	$('.dtc').click(function(e){
		var turn = $(this).data("t")
		if (turn != undefined){
            stop_replay();
            cur_turn = turn;
			show_turn(turn);
		}
	});
    
    cur_turn=last;
    
    var r = parse_query_string(window.location.href);
    var s = undefined;
    if (r && r["s"]){
        s = r["s"];
    }
    
    var no_scroll = true;
    if (s){
        s = parseInt(s);
        if (s > 0 && s < last){
            cur_turn = s;
            no_scroll  = undefined;
        }
    }
    
    show_turn(cur_turn, no_scroll);
    
    
});
</script>



<div id="left_block">
	
	<div id="hero1">
		
			<div id="hero1_info" class="box hide_on_scroll">
				
<div class="block">
  <div class="block_h">
	
		Герой  
	
	</div>  
  <div class="new_line">
    <label class="l_capt">Имя</label>
    <div class="field_content">	
			
				Палундра
			
			            
		</div>
  </div>                

	
	  <div class="new_line">
	    <label class="l_capt">Бог</label>
	    <div class="field_content">
				<a href="/gods/%D0%90%D0%BD%D0%B3%D0%B5%D0%BB%D1%8A%20%D0%A1%D0%B2%D0%B5%D1%82%D0%B0">Ангелъ Света</a>
			</div>
	  </div>
	
	
	  <div class="new_line">
	    <label class="l_capt">Пол</label>
	    <div class="field_content">женский</div>
	  </div>
	
	
	  <div class="new_line">
	    <label class="l_capt">Возраст</label>
	    <div class="field_content">3 года 7 месяцев</div>
	  </div>
	
  <div class="new_line">
    <label class="l_capt">Девиз</label>
    <div class="field_content"><i>[Я] буду долго 💎!</i></div>
  </div>
	
    <div class="new_line">
      <span class="l_capt">Характер</span>
      <span class="field_content">нейтральный</span>
    </div>
	
  
    <div class="new_line">
      <label class="l_capt">Гильдия</label>
      <div class="field_content"> <i>
				
						<a href="/stats/guild/Каракрас" onclick="window.open(this.href);return false;" style="text-decoration:none;">Каракрас</a>
				
			</i></div>
    </div>
  
	<div class="new_line"></div>
</div>

			</div>
			<div id="hero1_stats" class="box">
				<div class="block">
  <div class="block_h">
		
			Данные героя
		
	
     
	</div>
  <div>
		
	    <div class="new_line">
	      <label class="l_capt">Уровень</label>
	      <div class="field_content">109</div>
	    </div>
	    <div class="new_line">
	      
    <div class="pr_bg" style="width:100%; background-color:#F9B436;" title="76%">
        <div id="pr_l" class="pr_bar" style="width:24%; margin-bottom: -12px;"></div>
    </div>
	    </div>  
		
    <div class="new_line">
      <label class="l_capt">Инвентарь</label>
      <div class="field_content">14 / 50</div>
    </div>
    <div class="new_line">
      
    <div class="pr_bg" style="width:100%; background-color:sienna;" title="28%">
        <div id="pr_q" class="pr_bar" style="width:72%; margin-bottom: -12px;"></div>
    </div>
    </div>
    <div class="new_line">
      <label class="l_capt">Здоровье</label>
      <div class="field_content"><span class='hpd' id='hpd0'></span><span id='hp0'>44</span> / <span id='hpm0'>288</span></div>
    </div>
    <div class="new_line">
      
      
    <div class="pr_bg" style="width:100%; background-color:red;" title="15%">
        <div id="hpp0" class="pr_bar" style="width:85%; margin-bottom: -12px;"></div>
    </div>
    </div>
    <div class="new_line">
      <label class="l_capt">Золота</label>
      <div class="field_content">около 11 тысяч</div>
    </div>

	
    <div class="new_line">
      <span class="l_capt">Смертей</span>
      <span class="field_content">105</span>
    </div>


    <div class="new_line">
      <span class="l_capt">Дуэли</span>
      <span class="field_content">1544 / 1591</span>
    </div>
	

	
		
			
			
		
	
	<div class="new_line"></div>
  </div>
</div>

			</div>			
			
			<div id="hero1_inventory" class="box hide_on_scroll">
				<div class="block">
  <div class="block_h">Инвентарь
    <img alt="Spinner" id="spinner_loo" src="/images/spinner.gif?1276954498" style="vertical-align:bottom; display: none;" />
  </div>
  <div id="inv_capt">
    
      <span>
        
          В карманах героя можно найти:
        
      </span>
      <ul>
        
          <li>            
						<span style=''>
              кровалол
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              трясогубку
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              краеугольный камень
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              второй носок
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              глюквенный морс
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              табличку «Не преклоняться!»
            </span>
            
          </li>
        
          <li>            
						<span style='font-weight:bold;'>
              ветвь власти
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              газету «Вестник Апокалипсиса»
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              рыбу фугу ре минор
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              рыбу голодного копчения
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              чпокуня
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              акт милосердия с печатью
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              таблетки от мщения
            </span>
            
          </li>
        
          <li>            
						<span style='font-weight:bold;'>
              именной грамотомет
            </span>
            
          </li>
        
      </ul>
    
  </div>
</div>
			</div>
		
        <div class='c_spacer'></div>
	</div>
			
</div>
<div id="central_block">
	<div class="box" id="last_items_arena">
		<script nonce='15c00b5250ddedaabc203b67f8b034fd'>
	function report_godvoice(voice_id, sid, link, t) {
        var c_text = 'Вы уверены, что этот глас нарушает правила хорошего тона?';
		if (typeof Ajax === 'undefined'){
			var element_id = '#av_c_'+voice_id;
			var ans =  confirm(c_text);
			if (ans){
			    $.post('/hero/report_arena_voice_m', {'id':voice_id, 'text':t, 'sid':sid, 'link':link}, function(data) {
						if (data && data['status'] == 'success'){
							$(element_id).addClass('cmplt_status');
						  $(element_id).html(data['text']);
							setTimeout(function(){
								$(element_id).hide();
							},5000);
						}
				});														
			}
			return false;
		}
		else {
			if (confirm(c_text)) { new Ajax.Request('/hero/report_arena_voice/'+voice_id+'?'+Object.toQueryString({'text':t, 'link':link, 'sid':sid}), {asynchronous:true, evalScripts:true}); }; return false;
		}
	}
</script>


	<div class="afl block">
	  <div class="block_h">
		
			  
		 
		
		
        Тренировочный бой (шаг <span id='turn_num'>25</span> / 25)

		
				
		
			
				<a href="/duels/log/lcfz7bchx?sort=desc" title="Обратная сортировка (нажмите, чтобы изменить порядок записей)">▼</a>

			
			
		

    <img alt="Spinner" id="spinner_ml" src="/images/spinner.gif?1276954498" style="vertical-align:bottom; display: none;" /> 
		
	  </div>
      <div class='d_content'>
		
  	      <div class="new_line dtc t1  " data-t="1" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:51
						<div class="d_turn">шаг 1</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Палундра и Инка Прунелей сошлись в воображаемом поединке!</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t2  " data-t="2" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:52
						<div class="d_turn">шаг 2</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Палундра демонстрирует свои кулинарные навыки, комкая противника как блин.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t3  " data-t="3" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:52
						<div class="d_turn">шаг 3</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Инка Прунелей пляшет на костях противника. Палундра стонет — это чертовски больно!</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t4  " data-t="4" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:52
						<div class="d_turn">шаг 4</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Палундра берёт себя в руки и с размаху бросает в противника.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t5  " data-t="5" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:53
						<div class="d_turn">шаг 5</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Точным ударом Инка Прунелей указывает противнику на брешь в снаряжении.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t6  " data-t="6" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:53
						<div class="d_turn">шаг 6</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Палундра старательно втирает урон кулаками в спину противника.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t7  " data-t="7" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:53
						<div class="d_turn">шаг 7</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Инка Прунелей ударила с оттягом. Палундра, получив по мозгам, не на шутку рассердилась.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t8  " data-t="8" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:54
						<div class="d_turn">шаг 8</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Сражённая могучим ударом соперника Инка Прунелей упала и отжалась.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t9  " data-t="9" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:54
						<div class="d_turn">шаг 9</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Получив подсечку, Палундра падает духом и телом.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t10  " data-t="10" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:54
						<div class="d_turn">шаг 10</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Палундра саданула сгоряча. Инка Прунелей легко увернулась, почти не пострадав.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t11  " data-t="11" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:55
						<div class="d_turn">шаг 11</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Инка Прунелей обнимает колени противника с неистовой силою.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t12  " data-t="12" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:55
						<div class="d_turn">шаг 12</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Палундра втаптывает в грязь идеалы своего противника вместе с ним самим.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t13  " data-t="13" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:56
						<div class="d_turn">шаг 13</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Инка Прунелей подмигнула противнику левым глазом и ударила по правому.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t14  " data-t="14" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:56
						<div class="d_turn">шаг 14</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Используя не очень хорошо отточенное умение «заразная зевота», Палундра оставляет на сопернике глубокие зазубрины.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t15  " data-t="15" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:56
						<div class="d_turn">шаг 15</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Инка Прунелей отпускает сальные шуточки. Палундра поскальзывается и больно бьётся головой.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t16  " data-t="16" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:57
						<div class="d_turn">шаг 16</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Палундра убеждает противника, что уж сегодня она точно под присмотром Великой. Инка Прунелей в панике взирает на небо и пропускает хук в челюсть.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t17  " data-t="17" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:57
						<div class="d_turn">шаг 17</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Инка Прунелей попыталась применить умение. Палундра хохочет — разве это «лучи любви»?</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t18  " data-t="18" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:57
						<div class="d_turn">шаг 18</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Палундра треснула со всей дури. Инка Прунелей едва не упала.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t19  " data-t="19" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:58
						<div class="d_turn">шаг 19</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Инка Прунелей находит точку опоры и переворачивает землю. Не удержавшись, Палундра пребольно ударяется о небо.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t20  " data-t="20" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:58
						<div class="d_turn">шаг 20</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Инка Прунелей теряет очки здоровья и слепо шарит по земле, не заметив, что на них наступила Палундра.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t21  " data-t="21" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:59
						<div class="d_turn">шаг 21</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Пока Палундра считала ворон, Инка Прунелей подползла с тыла и сделала своё чёрное дело, убавив здоровья сопернику.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t22  " data-t="22" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:59
						<div class="d_turn">шаг 22</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Палундра махнула красной тряпкой перед лицом врага и нанесла из-под неё подлый удар.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t23  " data-t="23" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">12:59
						<div class="d_turn">шаг 23</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Инка Прунелей выписывает противнику семь плетей за дерзость и щелбан за наглость.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t24  " data-t="24" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">13:00
						<div class="d_turn">шаг 24</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Подбирая оптимальный градус отношений, Палундра бросает противника то в жар, то в холод.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t25  " data-t="25" style=''>
					<div class="d_capt">13:00
						<div class="d_turn">шаг 25</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Палундра заканчивает воображаемый поединок победителем. Инка Прунелей благодарит противника за зарядку для ума.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
		
		
		<div style="margin-bottom:0.2em;clear:both;"></div>
      </div>
	</div>

                                                                                                                                
	</div>
  


</div>
<div id="right_block">
	
		<div id="hero2">	
		
			<div id="hero2_info" class="box hide_on_scroll">
				
<div class="block">
  <div class="block_h">
	
		Герой  
	
	</div>  
  <div class="new_line">
    <label class="l_capt">Имя</label>
    <div class="field_content">	
			
				Инка Прунелей
			
			            
		</div>
  </div>                

	
	  <div class="new_line">
	    <label class="l_capt">Бог</label>
	    <div class="field_content">
				<a href="/gods/Januar">Januar</a>
			</div>
	  </div>
	
	
	  <div class="new_line">
	    <label class="l_capt">Пол</label>
	    <div class="field_content">женский</div>
	  </div>
	
	
	  <div class="new_line">
	    <label class="l_capt">Возраст</label>
	    <div class="field_content">6 месяцев 22 дня</div>
	  </div>
	
  <div class="new_line">
    <label class="l_capt">Девиз</label>
    <div class="field_content"><i>However fa[R] away...</i></div>
  </div>
	
    <div class="new_line">
      <span class="l_capt">Характер</span>
      <span class="field_content">миролюбивый</span>
    </div>
	
  
    <div class="new_line">
      <label class="l_capt">Гильдия</label>
      <div class="field_content"> <i>
				
						<a href="/stats/guild/Resistance" onclick="window.open(this.href);return false;" style="text-decoration:none;">Resistance</a>
				
			</i></div>
    </div>
  
	<div class="new_line"></div>
</div>

			</div>
			<div id="hero2_stats" class="box">
				<div class="block">
  <div class="block_h">
		
			Данные героя
		
	
     
	</div>
  <div>
		
	    <div class="new_line">
	      <label class="l_capt">Уровень</label>
	      <div class="field_content">48</div>
	    </div>
	    <div class="new_line">
	      
    <div class="pr_bg" style="width:100%; background-color:#F9B436;" title="59%">
        <div id="pr_l" class="pr_bar" style="width:41%; margin-bottom: -12px;"></div>
    </div>
	    </div>  
		
    <div class="new_line">
      <label class="l_capt">Инвентарь</label>
      <div class="field_content">25 / 31</div>
    </div>
    <div class="new_line">
      
    <div class="pr_bg" style="width:100%; background-color:sienna;" title="80%">
        <div id="pr_q" class="pr_bar" style="width:20%; margin-bottom: -12px;"></div>
    </div>
    </div>
    <div class="new_line">
      <label class="l_capt">Здоровье</label>
      <div class="field_content"><span class='hpd' id='hpd1'></span><span id='hp1'>1</span> / <span id='hpm1'>288</span></div>
    </div>
    <div class="new_line">
      
      
    <div class="pr_bg" style="width:100%; background-color:red;" title="0%">
        <div id="hpp1" class="pr_bar" style="width:100%; margin-bottom: -12px;"></div>
    </div>
    </div>
    <div class="new_line">
      <label class="l_capt">Золота</label>
      <div class="field_content">около 24 тысяч</div>
    </div>

	
    <div class="new_line">
      <span class="l_capt">Смертей</span>
      <span class="field_content">24</span>
    </div>


    <div class="new_line">
      <span class="l_capt">Дуэли</span>
      <span class="field_content">24 / 15</span>
    </div>
	

	
		
			
			
		
	
	<div class="new_line"></div>
  </div>
</div>

			</div>
			
			<div id="hero2_inventory" class="box hide_on_scroll">
				<div class="block">
  <div class="block_h">Инвентарь
    <img alt="Spinner" id="spinner_loo" src="/images/spinner.gif?1276954498" style="vertical-align:bottom; display: none;" />
  </div>
  <div id="inv_capt">
    
      <span>
        
          В карманах героя можно найти:
        
      </span>
      <ul>
        
          <li>            
						<span style='font-weight:bold;'>
              черпак вдохновения
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              подсолнечного зайца
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              палку-ковырялку
            </span>
            
          </li>
        
          <li>            
						<span style='font-weight:bold;'>
              божка из машинки
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              молотую чушь
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              чесночную жвачку
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              амфибиотик
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              доминошную рыбу (2 шт)
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              заливное «Злые язычки»
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              пирожное с кремом от загара
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              табличку «Лежачих не бить»
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              права категории «А?»
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              сапёрышко
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              полётную карту шмеля
            </span>
            
          </li>
        
          <li>            
						<span style='font-weight:bold;'>
              лопату для самокопания
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              ракетный отражатель
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              чистую культуру барабанной палочки
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              порцию дожирака
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              горшочек эктоплазмы
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              переносной очаг возгорания
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              рандомный трофей
            </span>
            
          </li>
        
          <li>            
						<span style='font-weight:bold;'>
              мешок с трофеями
            </span>
            
          </li>
        
          <li>            
						<span style='font-weight:bold;'>
              свиток депортации
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              карту потери памяти
            </span>
            
          </li>
        
      </ul>
    
  </div>
</div>
			</div>						
		</div>	
		
        <div class='c_spacer'></div>
	</div>	
			
</div>


	</div>
    
	
	<div class="lastduelpl_f">
		<div>Хроники хранятся не менее 30 дней.</div>
	</div>
	
	
</div>


</div>

</body>`;