import * as tournaments from '../tournaments';
import { TEST_REPORT1 } from './data';
tournaments.setInv({
    "хвост удачи": 1,
    "пособие «Как приручить рандом»": 1,
    "рандомизатор выигрышей на арене": 1,
    "рандомный трофей": 2,
    "рандомоотвод": 1,
    "яблоко рандома": 1,
    "полоску везения": 1,
    "табличку «Мне повезёт!»": 1,
    "13-листный клевер": 1,
});
describe('Tournaments', () => {
    test('Gets winner', () => {
        const battleBlank: tournaments.BattleData = {
            url: "",
            id: "",
            status: "test"
        };
        const root = tournaments.getRoot(TEST_REPORT1);
        const battle = tournaments.parseBattle(jQuery(root as any), battleBlank);
        expect(battle.winner).toBe("Палундра");
    });
    test('Gets gifts', () => {
        const battleBlank: tournaments.BattleData = {
            url: "",
            id: "",
            status: "test"
        };
        const root = tournaments.getRoot(TEST_REPORT1);
        const battle = tournaments.parseBattle(jQuery(root as any), battleBlank);
        expect(battle.inv1).toBe("0");
        expect(battle.inv2).toBe("2");
    });
    test('Gets hp', () => {
        const battleBlank: tournaments.BattleData = {
            url: "",
            id: "",
            status: "test"
        };
        const root = tournaments.getRoot(TEST_REPORT1);
        const battle = tournaments.parseBattle(jQuery(root as any), battleBlank);
        expect(battle.hp1).toBe("44");
        expect(battle.hpm1).toBe("288");
        expect(battle.hp2).toBe("1");
        expect(battle.hpm2).toBe("288");//yes its same max hp, sure
    });
})
