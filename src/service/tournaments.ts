import axios from 'axios';
// import { getExample } from './item';

const canonicalGV = "godville.net/duels/log/"
const canonicalER = "gv.erinome.net/duels/log/";
const parseRegex = /(.*)(https?:\/\/(godville\.net|gv\.erinome\.net)\/duels\/log\/([A-z0-9]+))[#?/]?(.*)/i;

export interface BattleData {
    id: string;
    url: string;
    status: string;
    reason?: string;
    time?: string;
    god1?: string;
    god2?: string;
    hero1?: string;
    hero2?: string;
    guild1?: string;
    guild2?: string;
    hp1?: string;
    hp2?: string;
    hpm1?: string;
    hpm2?: string;
    inv1?: string;
    inv2?: string;
    winner?: string;
}
export interface HeroData {
    name: string;
    inv: string;
    hp: string;
    maxHp: string;
    god: string;
    guild: string;
}
export const getRoot = (data: string) => {
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(data, 'text/html');
    return htmlDoc.getElementById("wrap");
};

const getLinkData = (link: string): string[] => link.match(parseRegex) || [];
export const parseLinks = (links: string): string[] => links.split("\n")
    .filter(a => a && ((a.indexOf(canonicalER) !== -1) || (a.indexOf(canonicalGV) !== -1)))
    .map(a => {
        const match = getLinkData(a);
        return (match && match[2]) || "";
    })
    .filter(a => a.length > 0)
    .map(l => l.replace("http://", "https://"));

const getBattle = (link: string): BattleData => {
    const match = getLinkData(link);
    if (!(match && match[2] && match[4])) {
        return {
            url: link,
            id: link,
            status: "error",
            reason: "Ссылка не валидна"
        };
    }
    return {
        url: match[2],
        id: match[4],
        status: "new"
    };
}
const getGod = (data: JQuery<HTMLElement>): HeroData => {
    const hero = {
        name: "",
        inv: "",
        hp: "",
        maxHp: "",
        god: "",
        guild: "",
    };
    hero.inv = data.find("#inv_capt ul").text();
    data.find("#hero1_info > .block .new_line, #hero2_info > .block .new_line").each(function () {
        const bd = jQuery(this as any);
        switch (bd.find(".l_capt").text()) {
            case "Имя":
                hero.name = bd.find(".field_content").text().trim();
                break;
            case "Бог":
                hero.god = bd.find(".field_content").text().trim();
                break;
            case "Гильдия":
                hero.guild = bd.find(".field_content").text().trim();
                break;
            case "Пол":
            case "Возраст":
            case "Характер":
            case "Девиз":
                break;
        }
    });
    hero.hp = data.find("#hp0,#hp1").text();
    hero.maxHp = data.find("#hpm0,#hpm1").text();

    return hero;
};
function er(str: string) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
export let invMatch: { [index: string]: number } = {};
let regex = new RegExp('^$');
const calcRegexp = () => {
    regex = new RegExp(`.*(${Object.keys(invMatch).map(er).join("|")}).*`, "gi");
}
export const setInv = (data: {[index: string]: number})=>{
    invMatch = data;
    calcRegexp();
};
setInv({
    "триббл": 1,
});
const countMatch = (str: string) => str.split("\n").filter(a => a && a.length && a.match(regex)).reduce(
    (count, item) => {
        Object.keys(invMatch).forEach((trophy) => {
            if (item.match(new RegExp(`.*(${er(trophy)}).*`, "gi"))) {
                count += (invMatch[trophy] || 0);
            }
        })
        return count;
    }, 0
)
export const parseBattle = (data: JQuery<HTMLElement>, battle: BattleData): BattleData => {
    battle.status = "success";
    battle.time = data.find(".ft").text();
    const god1 = getGod(data.find("#hero1"));
    const god2 = getGod(data.find("#hero2"));

    battle.god1 = god1.god;
    battle.hero1 = god1.name;
    battle.hp1 = god1.hp;
    battle.hpm1 = god1.maxHp;
    battle.guild1 = god1.guild;

    battle.god2 = god2.god;
    battle.hero2 = god2.name;
    battle.hp2 = god2.hp;
    battle.hpm2 = god2.maxHp;
    battle.guild2 = god2.guild;

    battle.inv1 = countMatch(god1.inv).toString();
    battle.inv2 = countMatch(god2.inv).toString();
    if (battle.hp1 === "1" && battle.hp2 === "1") {
        const messages = data.find(".afl .new_line .t");
        const lastMessage = jQuery(messages[messages.length - 1]).text();
        const index1 = lastMessage.indexOf(god1.name);
        const index2 = lastMessage.indexOf(god2.name);
        if ((index1 !== -1) && (index2 !== -1)) {
            if (index1 > index2) {
                battle.winner = god2.name;
            } else {
                battle.winner = god1.name;
            }
        }
    } else if (battle.hp1 === "1") {
        battle.winner = god2.name;
    } else {
        battle.winner = god1.name;
    }
    return battle;
};
export const tournament = (link: string) => new Promise<BattleData>((resolve) => {
    const battle = getBattle(link);
    if (battle.status === "error") {
        resolve(battle);
        return;
    }
    // getExample().then((response) => {
    axios.get(battle.url).then((response) => {
        if (response.status !== 200) {
            battle.status = "error";
            battle.reason = "Не удалось загрузить репорт";
            resolve(battle);
            return;
        }
        const root = getRoot(response.data);
        if (!root) {
            battle.status = "error";
            battle.reason = "Контент страницы не валидный";
            resolve(battle);
            return;
        }
        resolve(
            parseBattle(jQuery(root as HTMLElement), battle)
        );
    }).catch(err => {
        battle.status = "error";
        battle.reason = "Не удалось выполнить запрос в Годвиль";
        resolve(battle);
        return;
    });
});
