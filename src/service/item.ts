
const DATA = `
<!DOCTYPE html>
<html>
<head>
	  
<title>Хроника Годвилля</title>	

<script type="text/javascript" src="/javascripts/jquery-1.12.4.min.js" ></script>
<script src="/javascripts/awardj.js?1496979418" type="text/javascript"></script>
<script src="/javascripts/params_parse.js?1568892649" type="text/javascript"></script>

<link href="/stylesheets/common.css?1581510012" media="screen" rel="stylesheet" type="text/css" />
<link href="/stylesheets/common-mini.css?1535888562" media="screen" rel="stylesheet" type="text/css" />
<link href="/stylesheets/progress_bar.css?1299941084" media="screen" rel="stylesheet" type="text/css" />
<link href="/stylesheets/hero_log.css?1607387784" media="screen" rel="stylesheet" type="text/css" />
<META NAME="gvdt" CONTENT="hero_log_n"><script src="/javascripts/nt.js?1509365639" type="text/javascript"></script>
<script src="/javascripts/st.js?1459925068" type="text/javascript"></script><script src="/javascripts/stjq.js?1581510012" type="text/javascript"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-1675968-2"></script>
    <script src="/javascripts/garu.js" type="text/javascript"></script>


<meta name="keywords" content="Годвилль, Godville, Zero Player Game, ZPG, ZRPG, самоиграйка, тамагочи, браузерная игра, божественная комедия"><meta name="description" content="Божественная комедия-самоиграйка с участием бога и, кхм, героя. А еще MMO и ZPG, если вы знаете эти слова." />
<link rel="shortcut icon" href="/images/favicon.ico"/>
<script nonce='2a79ea27c279e471f4d180b08d62b00a'>
(function () {
    $(document).ready(function() {
        
        var r = parse_query_string(window.location.href);
        if (r && r["u"] == "1"){
            window.location = removeURLParameter("u", window.location.href);
        }
        
    });
}());
</script>
<script nonce='2a79ea27c279e471f4d180b08d62b00a'>
function to_mobile(){
	$.post('/hero/to_mobile', {}, function(data) {
		location.reload();
	});
}
$(document).ready(function() {
    $('.to_mobile').click(function(){
        to_mobile();
        return false;
    });
    $('.to_top').click(function(e){
        $(window).scrollTop(0);
        return false;
    })
    $('#reload').click(function(e){
        location.reload(true)
        return false;
    });
    $('a[data-rv]').click(function(e){
        var v = $(this).data('rv');
        report_godvoice(v[0],v[1],v[2],v[3]);
        return false;
    })
    $('.n_wn').click(function(e){
        window.open(this.href);
        return false;
    })
	if (typeof(jq_mfp_init) != 'undefined'){
		jq_mfp_init();
	}    
});
</script>

</head>
<body>
<div id="wrap">


<div id="page_wrapper">
	<div class="lastduelpl">
	
		<span><a href="/duels/log/rqnyeepy4">Тренировка</a></span>
	
	</div>
    
	
	<div class='ft'>01.03.2021 18:46 </div>
	
    
	
	<div class="lastduelpl">
		
			
		
	</div>
	
	
	<div id="arena_block" style="float: left; width: 100%;">
			<div id="p_status" style="text-align:center;min-height:15px;"></div>
	<div style="text-align:center;display: none;" id="app_bar">
		
			<span id="app_status">Подключаемся к сервису быстрых обновлений...</span>
			<span title="Скрыть"><a href="#" onclick="Effect.toggle(&quot;app_bar&quot;,'blind',{});; return false;">[X]</a></span>		
		
	</div>





<div class='rpl'>
    <div class='rpl_tc' style='display:none;'></div>
<input id='slider' type="range"  min="1" max="24" value="24" data-role="none"/>
<span id='sback' title='Предыдущий ход (стрелка влево)'>⏪&#xFE0E;</span>
<span id='splay' title='Проигрывание/пауза (Enter)'>&#x25b6;&#xFE0E;</span>
<span id='sps' title='Проигрывание/пауза (Enter)' style='display:none;'>⏸&#xFE0E;</span>
<span id='sfwd' title='Следующий ход (стрелка вправо)'>⏩&#xFE0E;</span>
</div>


<script nonce='2a79ea27c279e471f4d180b08d62b00a'>

$(document).ready(function() {
    var hp = {"-1":{"0":340,"1":340},"0":{"0":340,"1":340},"1":{"0":340,"1":340},"2":{"0":313,"1":340},"3":{"0":313,"1":299},"4":{"0":290,"1":299},"5":{"0":290,"1":260},"6":{"0":252,"1":260},"7":{"0":252,"1":226},"8":{"0":213,"1":226},"9":{"0":213,"1":198},"10":{"0":192,"1":198},"11":{"0":208,"1":181},"12":{"0":180,"1":181},"13":{"0":180,"1":140},"14":{"0":154,"1":140},"15":{"0":154,"1":108},"16":{"0":124,"1":108},"17":{"0":124,"1":84},"18":{"0":96,"1":84},"19":{"0":96,"1":52},"20":{"0":58,"1":52},"21":{"0":58,"1":33},"22":{"0":34,"1":33},"23":{"0":34,"1":1},"24":{"0":34,"1":1}};    
    var moves = [];
    var d_maps = [];
    
    var last= parseInt($('#turn_num').text());
    
    
    var play_timer = undefined;
    var cur_turn;
            
	var scroll_to = function(element){
        if (element.length == 0){
            return;
        }

		var elHeight = 0;
		element.each(function() {
		  elHeight += $(this).height();
		});        
        
        var offset = element.offset().top + elHeight;
            if(!element.is(":visible")) {
                element.css({"visibility":"hidden"}).show();
                var offset = element.offset().top;
                element.css({"visibility":"", "display":""});
            }

            var visible_area_start = $(window).scrollTop();
            var visible_area_end = visible_area_start + window.innerHeight;

            if(offset < visible_area_start || offset > visible_area_end){
                 // Not in view so scroll to it
                 $('html,body').animate({scrollTop: offset - window.innerHeight/3}, 300);
                 return false;
            }
            return true;
	}        
    
    var map_scroll = function(){
        
        if ($('.dmh').length == 0){
            return;
        }
        
        var m_width = $('.dmh').width();
        var m_left = $('.dmh').position().left;
        
        var map_width = $('#dmap').width();
        
        var s_left = $('#dmap').scrollLeft();
        if ( s_left < m_left || 
            s_left > m_left
        ) {
            $('#dmap').scrollLeft( m_left - map_width / 2);
//            console.log('dungeon map scrolled ' + s_left + ' ' + m_left);
        }
    }

    
    var is_out_of_view = function(element){
        if (element.length == 0){
            return;
        }
        var elementTop = element.offset().top;
        var elementBottom = element.outerHeight();

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        if (viewportTop > elementTop) {
//        if (elementBottom < viewportBottom ){
            return true;
        }
        else {
            return false;
        }
    }
        
    var add_on_scroll = function(el_id){
        if ($(el_id).length > 0){
            ($(el_id).parent().parent()).addClass('bldn');
        }
    }

    var on_scroll = function(el_id){
        var fixed_mode = undefined;
        var dmap = $($('.bldn')[0]);
        var covered = is_out_of_view($(dmap.parent()));
        if (!dmap.hasClass('block_fixed')){
            if (covered && $('.d_content').innerHeight() > window.innerHeight){
                fixed_mode = true;
            }
        }
        else if (!covered) {
            fixed_mode = false
        }
        if (fixed_mode){
            $('.bldn').each(function(index){
                $(this).addClass('block_fixed');
                $(this).offset({'left':$(this).parent().offset().left});
            });
            $('.hide_on_scroll').hide();
        }
        else if (fixed_mode == false) {
            $('.bldn').each(function(index){
                $(this).removeClass('block_fixed');
                $(this).removeAttr('style');
            });
            $('.hide_on_scroll').show();
        }
    }
    
    add_on_scroll('#dmap');
    add_on_scroll('#alls');
    add_on_scroll('#opps');
    add_on_scroll($('#hpp1').parent().parent().parent());
    add_on_scroll($('#hpp0').parent().parent().parent());

    $(window).on('resize scroll', function() {
        on_scroll();
    });        
    
    
    var draw_map = function(turn_num){
      turn_num = turn_num;
      if (d_maps == undefined || d_maps.length == 0){
        return;
      }
      
      var d_map = d_maps[turn_num];
      if (d_map == undefined){
        return;
      }
      var dm = $('#dmap').empty();
      var move = moves[turn_num];
      for (var y=0;y<d_map.length;y++){
        var l = d_map[y];
        var hl = $('<div></div>').addClass('dml').appendTo(dm);

        hl.css('width', l.length*21);
        for (var x=0;x<l.length;x++){
          var v = l[x];
          var cl = $('<div></div>').css({'left': 21*x});
          if (v != 'f'){
            cl.text(v).addClass('dmc');
            
            if (v == '#'){
              cl.addClass('dmw');
            }
            else if (v == '@'){
              cl.addClass('dmh');
            }
            
          }
          cl.appendTo(hl);
        }
      }
    };
    
    var show_turn = function(turn_num, no_scroll){
        
        if (no_scroll == undefined){
            scroll_to($('.t'+turn_num));
        }        
        
        
        $('.ct').removeClass('ct ctbg');
        $('.t'+turn_num).addClass('ct ctbg');
        
        draw_map(turn_num);


        var thp = hp[turn_num];
        var thpp = hp[turn_num-1];
        
        if (thp){
            for (var key in thp) {
                var hpd = undefined;
                var h = $('#hp'+key);
                if (h.length > 0){
                    h.text(thp[key]);
                }
                if (thpp[key] != undefined){
                    hpd = thp[key] - thpp[key];
                }
                
                var h_p = $('#hpd'+key).text('');
                if (hpd){
                    var h_p = $('#hpd'+key);
                    if (h_p.length > 0){
                        h_p.removeClass('hpd_green');
                        h_p.removeClass('hpd_red');
                        if (hpd > 0){
                            h_p.text('+'+(hpd));
                            h_p.addClass('hpd_green');
                        }
                        else {
                            h_p.text((hpd));
                            h_p.addClass('hpd_red');
                        }
                    }
                }
                
                
                if (h_p.length > 0){
                    if (thp[key] == undefined) {
                        h_p.parent().parent().hide();
                    }
                    else {
                        h_p.parent().parent().show();
                    }
                }

                var hpp = $('#hpp'+key);
                if (hpp.length > 0){
                    var hpm = $('#hpm'+key).text();
                    var prct = (thp[key]/hpm*100).toFixed();
                    hpp.css('width', (100 - prct)+'%');
                    
                    var p = hpp.parent();
                    
                    var pc_color = 'green';
                    if (prct < 30 ){
                       pc_color = 'red'
                    }
                    else if (prct < 70){
                      pc_color = 'yellow'
                    }
                    p.css('background-color', pc_color);
                    p.attr('title', prct+'%')
                }
            }
        }
        
        $('#turn_num').text(turn_num);
        $('#slider').val(turn_num);
        if (!no_scroll){
            update_location("s", turn_num);
        }
        
        map_scroll();        
        if (d_maps.length == 0){
          $('.dmh').removeClass('dmh');
          if (moves.length > 0){
              $('#'+moves[turn_num-1]).addClass('dmh');
          }
        }
        if (turn_num == last){
//            $('#sfwd').addClass('rpld');
            $('.dt').show();
        }
        else {
            // $('#sfwd').removeClass('rpld');
            $('.dt').hide();
        }
        if (turn_num == 1){
            // $('#sback').addClass('rpld');
        }
        else {
            // $('#sback').removeClass('rpld');
        }
        return false;
    }
        
	var stop_replay = function(){
		if (play_timer != undefined){
			clearInterval(play_timer);
			play_timer = undefined;
		}
        $('#sps').hide();
        $('#splay').show();        
	}
    
	var play_step_func = function(){
		show_turn(cur_turn);
		
		if (cur_turn == last){
            stop_replay();
            cur_turn = 1;
			return;
		}
        cur_turn++;
	}
    
    var back_f = function(){
        if (cur_turn== 1){
            cur_turn = last+1;
        }
        stop_replay();
        if (cur_turn > 1){
            cur_turn--;
            show_turn(cur_turn);
        }        
    }
    
	var forward_f = function(){
        if (cur_turn==last){
            cur_turn = 0;
        }				        
		stop_replay();
        if (cur_turn < last){
            cur_turn++;
            show_turn(cur_turn);
        }        
    }
    
    var play_f = function(){
        stop_replay();
        if (cur_turn==last){
            cur_turn = 1;
        }
        play_timer = setInterval(play_step_func, 1000);
        play_step_func();
        $('#splay').hide();
        $('#sps').show();        
    }
    
    
    $('#slider').change(function(e) {
        cur_turn = $(this).val();
        e.preventDefault();
        $('.rpl_tc').hide();
        show_turn(cur_turn);
        return false;
    });
    
    $('#slider').on('input', function(event){
        $('.rpl_tc').show();
        $('.rpl_tc').text(event.target.value);
    });    
    
    $('#sback').click(function(e){
        e.preventDefault();
        back_f();
        return false;
    });
    $('#sfwd').click(function(e){
        e.preventDefault();
        forward_f();
        return false;
    });
    
    $('#splay').click(function(e){
        e.preventDefault();
        play_f();
        return false;
    });
    $('#sps').click(function(e){
        e.preventDefault();
        stop_replay()
        return false;;
    });
    
	$("body").keydown(function(e){
        var handled = false;
		if (!e.ctrlKey && !e.altKey && !e.shiftKey && ((e.keyCode || e.which) == 37)) { // left arrow
            handled = true;
			back_f();
		}
		else if (!e.ctrlKey && !e.altKey && !e.shiftKey && ((e.keyCode || e.which) == 39)){ // right arrow
            handled = true;
			forward_f();
		}
		else if ((e.keyCode || e.which) == 13 && !e.ctrlKey && !e.altKey && !e.shiftKey ){ // enter
            handled = true;
			if ($('#splay').css('display') != 'none'){
				play_f();
			}
			else {
				stop_replay();
			}
		}
        if (handled){
            e.preventDefault();
            return false;
        }
        return true;
	});
    
	$('.dtc').click(function(e){
		var turn = $(this).data("t")
		if (turn != undefined){
            stop_replay();
            cur_turn = turn;
			show_turn(turn);
		}
	});
    
    cur_turn=last;
    
    var r = parse_query_string(window.location.href);
    var s = undefined;
    if (r && r["s"]){
        s = r["s"];
    }
    
    var no_scroll = true;
    if (s){
        s = parseInt(s);
        if (s > 0 && s < last){
            cur_turn = s;
            no_scroll  = undefined;
        }
    }
    
    show_turn(cur_turn, no_scroll);
    
    
});
</script>



<div id="left_block">
	
	<div id="hero1">
		
			<div id="hero1_info" class="box hide_on_scroll">
				
<div class="block">
  <div class="block_h">
	
		Герой  
	
	</div>  
  <div class="new_line">
    <label class="l_capt">Имя</label>
    <div class="field_content">	
			
				Меллия
			
			            
		</div>
  </div>                

	
	  <div class="new_line">
	    <label class="l_capt">Бог</label>
	    <div class="field_content">
				<a href="/gods/%D0%94%D0%B5%D1%80%D0%B8%D1%8F">Дерия</a>
			</div>
	  </div>
	
	
	  <div class="new_line">
	    <label class="l_capt">Пол</label>
	    <div class="field_content">женский</div>
	  </div>
	
	
	  <div class="new_line">
	    <label class="l_capt">Возраст</label>
	    <div class="field_content">11 лет 1 месяц</div>
	  </div>
	
  <div class="new_line">
    <label class="l_capt">Девиз</label>
    <div class="field_content"><i>Все м✵нстЯы по колено[X]</i></div>
  </div>
	
    <div class="new_line">
      <span class="l_capt">Характер</span>
      <span class="field_content">нейтральный</span>
    </div>
	
  
    <div class="new_line">
      <label class="l_capt">Гильдия</label>
      <div class="field_content"> <i>
				
						<a href="/stats/guild/Недонаторы" onclick="window.open(this.href);return false;" style="text-decoration:none;">Недонаторы</a>
				
			</i></div>
    </div>
  
	<div class="new_line"></div>
</div>

			</div>
			<div id="hero1_stats" class="box">
				<div class="block">
  <div class="block_h">
		
			Данные героя
		
	
     
	</div>
  <div>
		
	    <div class="new_line">
	      <label class="l_capt">Уровень</label>
	      <div class="field_content">130</div>
	    </div>
	    <div class="new_line">
	      
    <div class="pr_bg" style="width:100%; background-color:#F9B436;" title="46%">
        <div id="pr_l" class="pr_bar" style="width:54%; margin-bottom: -12px;"></div>
    </div>
	    </div>  
		
    <div class="new_line">
      <label class="l_capt">Инвентарь</label>
      <div class="field_content">8 / 50</div>
    </div>
    <div class="new_line">
      
    <div class="pr_bg" style="width:100%; background-color:sienna;" title="16%">
        <div id="pr_q" class="pr_bar" style="width:84%; margin-bottom: -12px;"></div>
    </div>
    </div>
    <div class="new_line">
      <label class="l_capt">Здоровье</label>
      <div class="field_content"><span class='hpd' id='hpd0'></span><span id='hp0'>34</span> / <span id='hpm0'>340</span></div>
    </div>
    <div class="new_line">
      
      
    <div class="pr_bg" style="width:100%; background-color:red;" title="10%">
        <div id="hpp0" class="pr_bar" style="width:90%; margin-bottom: -12px;"></div>
    </div>
    </div>
    <div class="new_line">
      <label class="l_capt">Золота</label>
      <div class="field_content">ни одного</div>
    </div>

	
    <div class="new_line">
      <span class="l_capt">Смертей</span>
      <span class="field_content">400</span>
    </div>


    <div class="new_line">
      <span class="l_capt">Дуэли</span>
      <span class="field_content">538 / 651</span>
    </div>
	

	
		
			
			
		
	
	<div class="new_line"></div>
  </div>
</div>

			</div>			
			
			<div id="hero1_inventory" class="box hide_on_scroll">
				<div class="block">
  <div class="block_h">Инвентарь
    <img alt="Spinner" id="spinner_loo" src="/images/spinner.gif?1276954498" style="vertical-align:bottom; display: none;" />
  </div>
  <div id="inv_capt">
    
      <span>
        
          В карманах героя можно найти:
        
      </span>
      <ul>
        
          <li>            
						<span style=''>
              благие двести
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              эликсир здоровья с мякотью
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              шар-латан
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              поглотенце
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              гормон карьерного роста
            </span>
            
          </li>
        
          <li>            
						<span style='font-weight:bold;'>
              баллон с лёгким паром
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              краснокожую паспортину
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              часы-голоходики
            </span>
            
          </li>
        
      </ul>
    
  </div>
</div>
			</div>
		
        <div class='c_spacer'></div>
	</div>
			
</div>
<div id="central_block">
	<div class="box" id="last_items_arena">
		<script nonce='2a79ea27c279e471f4d180b08d62b00a'>
	function report_godvoice(voice_id, sid, link, t) {
        var c_text = 'Вы уверены, что этот глас нарушает правила хорошего тона?';
		if (typeof Ajax === 'undefined'){
			var element_id = '#av_c_'+voice_id;
			var ans =  confirm(c_text);
			if (ans){
			    $.post('/hero/report_arena_voice_m', {'id':voice_id, 'text':t, 'sid':sid, 'link':link}, function(data) {
						if (data && data['status'] == 'success'){
							$(element_id).addClass('cmplt_status');
						  $(element_id).html(data['text']);
							setTimeout(function(){
								$(element_id).hide();
							},5000);
						}
				});														
			}
			return false;
		}
		else {
			if (confirm(c_text)) { new Ajax.Request('/hero/report_arena_voice/'+voice_id+'?'+Object.toQueryString({'text':t, 'link':link, 'sid':sid}), {asynchronous:true, evalScripts:true}); }; return false;
		}
	}
</script>


	<div class="afl block">
	  <div class="block_h">
		
			  
		 
		
		
        Тренировочный бой (шаг <span id='turn_num'>24</span> / 24)

		
				
		
			
				<a href="/duels/log/rqnyeepy4?sort=desc" title="Обратная сортировка (нажмите, чтобы изменить порядок записей)">▼</a>

			
			
		

    <img alt="Spinner" id="spinner_ml" src="/images/spinner.gif?1276954498" style="vertical-align:bottom; display: none;" /> 
		
	  </div>
      <div class='d_content'>
		
  	      <div class="new_line dtc t1  " data-t="1" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:46
						<div class="d_turn">шаг 1</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Walenhtain и Меллия сошлись в воображаемом поединке!</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t2  " data-t="2" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:47
						<div class="d_turn">шаг 2</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Точным ударом Walenhtain намертво прикрепляет противника к поликлинике.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t3  " data-t="3" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:47
						<div class="d_turn">шаг 3</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Меллия кидает кубики и попадает противнику в оба глаза. Дубль!</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t4  " data-t="4" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:47
						<div class="d_turn">шаг 4</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Меллия стушёвывается, но Walenhtain наводит резкость и прицельно бьёт.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t5  " data-t="5" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:48
						<div class="d_turn">шаг 5</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Walenhtain мысленно планирует серию критических ударов. Меллия её проводит.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t6  " data-t="6" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:48
						<div class="d_turn">шаг 6</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Walenhtain ставит сопернику речь, походку и огромный фингал.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t7  " data-t="7" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:48
						<div class="d_turn">шаг 7</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Меллия просто бьёт противника. Окружающие в недоумении.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t8  " data-t="8" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:49
						<div class="d_turn">шаг 8</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Walenhtain проводит мастер-класс по применению умения «оклеветация», и Меллия прекрасно подходит в качестве наглядного пособия.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t9  " data-t="9" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:49
						<div class="d_turn">шаг 9</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Получив подсечку, Walenhtain падает духом и телом.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t10  " data-t="10" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:49
						<div class="d_turn">шаг 10</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Вспомнив о нормах приличия, Walenhtain вытирает ноги о противника.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t11  " data-t="11" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:50
						<div class="d_turn">шаг 11</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Нетрадиционно применяя умение «морская болезнь», Меллия оторвала кусок полоски здоровья противника и приклеила его к своей.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t12  " data-t="12" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:50
						<div class="d_turn">шаг 12</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Walenhtain взмывает вверх в изящном прыжке и зависает в воздухе, широко раскинув руки. Меллия столь впечатлена зрелищем, что не успевает блокировать удар ногой в голову.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t13  " data-t="13" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:51
						<div class="d_turn">шаг 13</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Сражённый могучим ударом соперника Walenhtain упал и отжался.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t14  " data-t="14" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:51
						<div class="d_turn">шаг 14</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Walenhtain заявляет, что звезды сегодня на его стороне. Пока Меллия проверяет гороскоп, Walenhtain наносит ей сокрушительный удар в челюсть.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t15  " data-t="15" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:51
						<div class="d_turn">шаг 15</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Меллия заявляет, что гигиена превыше всего, и, поплевав на кулаки, заставляет противника умыться кровью.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t16  " data-t="16" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:52
						<div class="d_turn">шаг 16</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Walenhtain дружески подкалывает противника. Меллия мужественно страдает от аллергии на всё острое.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t17  " data-t="17" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:52
						<div class="d_turn">шаг 17</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Walenhtain встаёт в оппозицию, и Меллия немедленно бьёт его резиновой дубинкой по неправильному фуэте.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t18  " data-t="18" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:52
						<div class="d_turn">шаг 18</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Взорвавшись каскадом молниеносных движений, Меллия блокирует все возможные коварные удары, но получает простой, как валенок, удар по лбу.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t19  " data-t="19" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:53
						<div class="d_turn">шаг 19</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Меллия предъявляет свидетельство о выигрыше тендера на нанесение тяжких телесных повреждений.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t20  " data-t="20" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:53
						<div class="d_turn">шаг 20</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Меллия мысленно планирует серию критических ударов. Walenhtain её проводит.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t21  " data-t="21" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:54
						<div class="d_turn">шаг 21</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Меллия в новых технологиях не рубит, поэтому бьёт плашмя.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t22  " data-t="22" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:54
						<div class="d_turn">шаг 22</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Walenhtain освещает соперника фонарями из-под глаз. Меллия ослеплена и дезориентирована.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t23  " data-t="23" style='border-bottom: 1px dashed #888888;'>
					<div class="d_capt">18:54
						<div class="d_turn">шаг 23</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Подбирая оптимальный градус отношений, Меллия бросает противника то в жар, то в холод.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
  	      <div class="new_line dtc t24  " data-t="24" style=''>
					<div class="d_capt">18:55
						<div class="d_turn">шаг 24</div>
						</div>						
	        <div class="text_content ">
            
				  		<span class='t'>Меллия заканчивает воображаемый поединок победителем. Walenhtain благодарит противника за зарядку для ума.</span>
				
							
						
				 	</div>
				<div class="d_clear"></div>
	      </div>
	    
		
		
		<div style="margin-bottom:0.2em;clear:both;"></div>
      </div>
	</div>

                                                                                                                                
	</div>
  


</div>
<div id="right_block">
	
		<div id="hero2">	
		
			<div id="hero2_info" class="box hide_on_scroll">
				
<div class="block">
  <div class="block_h">
	
		Герой  
	
	</div>  
  <div class="new_line">
    <label class="l_capt">Имя</label>
    <div class="field_content">	
			
				Walenhtain
			
			            
		</div>
  </div>                

	
	  <div class="new_line">
	    <label class="l_capt">Бог</label>
	    <div class="field_content">
				<a href="/gods/Decster61">Decster61</a>
			</div>
	  </div>
	
	
	  <div class="new_line">
	    <label class="l_capt">Пол</label>
	    <div class="field_content">мужской</div>
	  </div>
	
	
	  <div class="new_line">
	    <label class="l_capt">Возраст</label>
	    <div class="field_content">2 года 2 месяца</div>
	  </div>
	
  <div class="new_line">
    <label class="l_capt">Девиз</label>
    <div class="field_content"><i>Прорвемс[Я] 💎 </i></div>
  </div>
	
    <div class="new_line">
      <span class="l_capt">Характер</span>
      <span class="field_content">беззлобный</span>
    </div>
	
  
    <div class="new_line">
      <label class="l_capt">Гильдия</label>
      <div class="field_content"> <i>
				
						<a href="/stats/guild/Каракрас" onclick="window.open(this.href);return false;" style="text-decoration:none;">Каракрас</a>
				
			</i></div>
    </div>
  
	<div class="new_line"></div>
</div>

			</div>
			<div id="hero2_stats" class="box">
				<div class="block">
  <div class="block_h">
		
			Данные героя
		
	
     
	</div>
  <div>
		
	    <div class="new_line">
	      <label class="l_capt">Уровень</label>
	      <div class="field_content">61</div>
	    </div>
	    <div class="new_line">
	      
    <div class="pr_bg" style="width:100%; background-color:#F9B436;" title="85%">
        <div id="pr_l" class="pr_bar" style="width:15%; margin-bottom: -12px;"></div>
    </div>
	    </div>  
		
    <div class="new_line">
      <label class="l_capt">Инвентарь</label>
      <div class="field_content">19 / 37</div>
    </div>
    <div class="new_line">
      
    <div class="pr_bg" style="width:100%; background-color:sienna;" title="51%">
        <div id="pr_q" class="pr_bar" style="width:49%; margin-bottom: -12px;"></div>
    </div>
    </div>
    <div class="new_line">
      <label class="l_capt">Здоровье</label>
      <div class="field_content"><span class='hpd' id='hpd1'></span><span id='hp1'>1</span> / <span id='hpm1'>340</span></div>
    </div>
    <div class="new_line">
      
      
    <div class="pr_bg" style="width:100%; background-color:red;" title="0%">
        <div id="hpp1" class="pr_bar" style="width:100%; margin-bottom: -12px;"></div>
    </div>
    </div>
    <div class="new_line">
      <label class="l_capt">Золота</label>
      <div class="field_content">около 10 тысяч</div>
    </div>

	
    <div class="new_line">
      <span class="l_capt">Смертей</span>
      <span class="field_content">55</span>
    </div>


    <div class="new_line">
      <span class="l_capt">Дуэли</span>
      <span class="field_content">24 / 24</span>
    </div>
	

	
		
			
			
		
	
	<div class="new_line"></div>
  </div>
</div>

			</div>
			
			<div id="hero2_inventory" class="box hide_on_scroll">
				<div class="block">
  <div class="block_h">Инвентарь
    <img alt="Spinner" id="spinner_loo" src="/images/spinner.gif?1276954498" style="vertical-align:bottom; display: none;" />
  </div>
  <div id="inv_capt">
    
      <span>
        
          В карманах героя можно найти:
        
      </span>
      <ul>
        
          <li>            
						<span style='font-weight:bold;'>
              бестактный двигатель
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              болтоукладчик
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              многозадачный задачник
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              мануал «Про изведение искусства»
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              рулон логов
            </span>
            
          </li>
        
          <li>            
						<span style='font-weight:bold;'>
              бриллиантовую муку
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              ногу со временем
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              подставку под горячую руку
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              ватно-марлевую каску
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              хомячий бивень
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              самоотклеивающийся скотч
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              тетрадку с прописными истинами
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              надувную совесть
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              выбитую дурь
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              шизоленту
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              равнобедренную кость
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              трактат «Оздоровительное пытание»
            </span>
            
          </li>
        
          <li>            
						<span style='font-weight:bold;'>
              филе босса Недоеда
            </span>
            
          </li>
        
          <li>            
						<span style=''>
              широкоформатную печать
            </span>
            
          </li>
        
      </ul>
    
  </div>
</div>
			</div>						
		</div>	
		
        <div class='c_spacer'></div>
	</div>	
			
</div>


	</div>
    
	
	<div class="lastduelpl_f">
		<div>Хроники хранятся не менее 30 дней.</div>
	</div>
	
	
</div>


</div>

</body>`;

export const getExample = () => Promise.resolve({
    status: 200,
    data: DATA
})