// ==UserScript==
// @name         Sansara Tournament Helper
// @namespace    http://tampermonkey.net/
// @version      {version}
// @description  Toolset for Godville tournament administrators
// @author       You
// @match        https://godville.net/superhero
// @grant        GM_getValue
// @require      https://gitlab.com/raddon222/sansara-tournament-helper/-/jobs/artifacts/master/raw/dist/index_bundle.js?job=build%20process#md5={hash}
// @grant GM_xmlhttpRequest
// ==/UserScript==

(function ($) {
  var assignItem = () => {
    var item = $('<div id="sth-root"></div>');
    item.prependTo(".superhero");
  };
  var loadBody = (target) => {
    if (target.STH) {
      target.STH.init();
      console.log("STH init done");
      return;
    }
    $(document).ready(() => {
      if (target.STH) {
        target.STH.init();
        console.log("STH init done");
      } else {
        console.log("STH data is missing");
      }
    });
    console.log("STH init started");
  };
  setTimeout(() => {
    assignItem();
    try {
      // eslint-disable-next-line no-undef
      loadBody(unsafeWindow);
    } catch (e) {
      loadBody(window);
    }
  }, 1000);
  // eslint-disable-next-line no-undef
})(jQuery);
